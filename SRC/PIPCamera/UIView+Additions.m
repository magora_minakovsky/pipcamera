//
//  UIView+Additions.m
//  PIPCamera
//
//  Created by Kirill Minskovskiy on 03.01.16.
//  Copyright © 2016 Kirill Minskovskiy. All rights reserved.
//

#import "UIView+Additions.h"

@implementation UIView (Additions)
@dynamic borderColor,borderWidth,cornerRadius;

- (void)setBorderColor:(UIColor *)borderColor {
    [self.layer setBorderColor:borderColor.CGColor];
    [self.layer setMasksToBounds:YES];
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    [self.layer setBorderWidth:borderWidth];
    [self.layer setMasksToBounds:YES];
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    [self.layer setCornerRadius:cornerRadius];
    [self.layer setMasksToBounds:YES];
}

+ (instancetype)createFromNib {
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] firstObject];
}

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

+ (UINib *)nib {
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

@end
