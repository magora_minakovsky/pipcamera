//
//  RecordVideoViewController.h
//  PIPCamera
//
//  Created by Kirill Minskovskiy on 03.01.16.
//  Copyright © 2016 Kirill Minskovskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^RecordVideoCompletion)(NSURL *filePath, NSError *error);

@interface RecordVideoViewController : UIViewController

@property (nonatomic, copy) NSURL *videoPath;
@property (nonatomic, copy) RecordVideoCompletion completion;
@property (assign, nonatomic) NSUInteger maxRecordDuration;

@end
