//
//  UIView+Additions.h
//  PIPCamera
//
//  Created by Kirill Minskovskiy on 03.01.16.
//  Copyright © 2016 Kirill Minskovskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Additions)

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;

+ (UINib *)nib;
+ (instancetype)createFromNib;
+ (NSString *)reuseIdentifier;

@end
