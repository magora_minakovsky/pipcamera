//
//  AppDelegate.h
//  PIPCamera
//
//  Created by Kirill Minskovskiy on 03.01.16.
//  Copyright © 2016 Kirill Minskovskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

