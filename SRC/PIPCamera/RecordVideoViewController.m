//
//  RecordVideoViewController.m
//  PIPCamera
//
//  Created by Kirill Minskovskiy on 03.01.16.
//  Copyright © 2016 Kirill Minskovskiy. All rights reserved.
//

#import "RecordVideoViewController.h"

#import "SCRecorder.h"

static void * CapturingStillImageContext = &CapturingStillImageContext;
static void * RecordingContext = &RecordingContext;
static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;


@interface RecordVideoViewController () <SCRecorderDelegate>

@property (nonatomic, strong) SCRecorder *recorder;
@property (nonatomic, weak) IBOutlet SCRecorderToolsView *previewView;

@end


@implementation RecordVideoViewController

- (SCRecorder *)recorder {
    if (_recorder == nil) {
        _recorder = [SCRecorder recorder];
    }
    return _recorder;
}

- (void)setMaxRecordDuration:(NSUInteger)maxRecordDuration {
    _maxRecordDuration = maxRecordDuration;
    
    self.recorder.maxRecordDuration = (self.maxRecordDuration != 0) ? CMTimeMake(self.maxRecordDuration, 1) : kCMTimeIndefinite;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.previewView.recorder = self.recorder;
    [self configureRecorder:self.recorder];
}

- (void)configureRecorder:(SCRecorder *)recorder {
    
    if (![recorder startRunning]) {
        NSLog(@"Something wrong there: %@", recorder.error);
    }
    
    // Set the AVCaptureSessionPreset for the underlying AVCaptureSession.
    recorder.captureSessionPreset = AVCaptureSessionPresetHigh;
    
    // Set the video device to use
    recorder.device = AVCaptureDevicePositionFront;
    
    // Listen to the messages SCRecorder can send
    recorder.delegate = self;
    
    // set video orientation
    recorder.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    
    // Get the video configuration object
    SCVideoConfiguration *video = recorder.videoConfiguration;
    
    video.enabled = YES;
    // The bitrate of the video videoп
    video.bitrate = 2000000; // 2Mbit/s
    // Size of the video output
    video.size = CGSizeMake(1280, 720);
    // Scaling if the output aspect ratio is different than the output one
    video.scalingMode = AVVideoScalingModeResizeAspectFill;
    // The timescale ratio to use. Higher than 1 makes a slow motion, between 0 and 1 makes a timelapse effect
    video.timeScale = 1;
    // Whether the output video size should be infered so it creates a square video
    video.sizeAsSquare = YES;
    
    // Get the audio configuration object
    SCAudioConfiguration *audio = recorder.audioConfiguration;
    
    // Whether the audio should be enabled or not
    audio.enabled = YES;
    // the bitrate of the audio output
    audio.bitrate = 128000; // 128kbit/s
    // Number of audio output channels
    audio.channelsCount = 1; // Mono output
    // The sample rate of the audio output
    audio.sampleRate = 0; // Use same input
    // The format of the audio output
    audio.format = kAudioFormatMPEG4AAC; // AAC
    
    // Get the photo configuration object
    SCPhotoConfiguration *photo = recorder.photoConfiguration;
    photo.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view layoutSubviews];
    
    self.previewView.backgroundColor = [UIColor clearColor];
    [self.previewView.layer setMasksToBounds:YES];
    self.recorder.previewLayer.frame = self.previewView.bounds;
    [self.previewView.layer addSublayer:self.recorder.previewLayer];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.recorder record];
}

@end
